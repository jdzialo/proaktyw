/*
 Version 1.3.2
 The MIT License (MIT)
 
 Copyright (c) 2014 Dirk Groenen
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 */

(function ($) {
    $.fn.viewportChecker = function (useroptions) {
        // Define options and extend with user
        var options = {
            classToAdd: 'visible',
            offset: 100,
            callbackFunction: function (elem) {}
        };
        $.extend(options, useroptions);

        // Cache the given element and height of the browser
        var $elem = this,
                windowHeight = $(window).height();

        this.checkElements = function () {
            // Set some vars to check with
            var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html'),
                    viewportTop = parseInt($(scrollElem).scrollTop()),
                    viewportBottom = parseInt(viewportTop + windowHeight);
            var prevTop = null;
            
            $elem.each(function (index) {
                var $obj = $(this);
                // If class already exists; quit
              var shown=false;
                if ($obj.hasClass('visible')) {
                    shown=true;
                    if(isScrolling)
                    return;
                }
                
                var elemTopO=parseInt(Math.round($obj.offset().top));
                // define the top position of the element and include the offset which makes is appear earlier or later
                var elemTop = parseInt(elemTopO + options.offset),
                        elemBottom = parseInt(elemTop + ($obj.height()));
                if (prevTop == elemTop)
                    return;
                prevTop = elemTop;
               
                // Add class if in viewport
//                console.log('elemTop' + elemTop + ' viewportBottom' + viewportBottom + ' elemBottom' + elemBottom + ' viewportTop' + viewportTop);
                if ((elemTop < viewportBottom) && (elemBottom > viewportTop)) {
//                    console.log('found');
                    if(!shown)
                    {
                    $obj.addClass(options.classToAdd);
                    }
                    if(!scrollingFocus)scrollingFocus=this;
                    if(!isScrolling&&scrollingFocus!=this)
                    {
                        
                       
                        var elemBottomO = parseInt(elemTopO + ($obj.height()));
                        
//                         console.log('elemTopO' + elemTopO + ' viewportBottom' + viewportBottom +
//                                 ' elemBottomO' + elemBottomO + ' viewportTop' + viewportTop);
                         
                        if (((elemBottomO-400 > viewportTop)&&(viewportTop > elemTopO))
                                    || ((elemTopO+400 < viewportBottom)&&(viewportBottom<elemBottomO))){
                                isScrolling=true;
                                 scrollingFocus=this;
                            $('html, body').animate({
                                scrollTop: $obj.offset().top
                            }, 200,null,function(){isScrolling=false;});
                            return;
                        }
                }
                    
                    // Do the callback function. Callback wil send the jQuery object as parameter
                    options.callbackFunction($obj);
//                    delete $elem[index];
                }
            });
        };

        // Run checkelements on load and scroll
        $(window).scroll(this.checkElements);
        this.checkElements();

        // On resize change the height var
        $(window).resize(function (e) {
            windowHeight = e.currentTarget.innerHeight;
        });
    };
})(jQuery);
var isScrolling=false;
var scrollingFocus=false;
