<?php

return [
    'title' => [
        'pages' => 'Strony',
        'create page' => 'Utwórz stronę',
        'create worker' => 'Utwórz pracownika',
        'create offer' => 'Utwórz ofertę',
        'edit page' => 'Edytuj stronę',
        'worker' => 'Pracownicy',
        'workers' => 'Pracownicy',
        'worker edit' => 'Edytuj pracownika',
        'offer' => 'Oferta',
        'offer edit' => 'Edytuj ofertę',
        'offers' => 'Oferty'
    ],
    'button' => [
        'create page' => 'Utwórz stronę',
        'create worker' => 'Utwórz pracownika',
        'create offer' => 'Utwórz ofertę',
    ],
    'table' => [
        'name' => 'Nazwa',
        'slug' => 'Slug',
        'body' => 'Treść',
        'content' => 'Zawartość',
    ],
    'form' => [
        'title' => 'Tytuł',
        'slug' => 'Slug',
        'meta_title' => 'Meta tytuł',
        'meta_description' => 'Meta opis',
        'og_title' => 'Facebook tytuł',
        'og_description' => 'Facebook opis',
        'og_type' => 'Facebook typ',
        'template' => 'Szablon strony',
        'is homepage' => 'Strona domowa ?',
        'body' => 'Treść',
        'content' => 'Zawartość',
        'name' => 'Nazwa'
    ],
    'validation' => [
        'attributes' => [
            'title' => 'title',
            'body' => 'body',
        ],
    ],
    'facebook-types' => [
        'website' => 'Strona',
        'product' => 'Produkt',
        'article' => 'Artykuł',
    ],
    'navigation' => [
        'back to index' => 'Wróć do indeksu stron',
    ],
    'offername is required' => 'Nazwa oferty jest wymagana.',
    'offerbody is required' => 'Ciało oferty jest wymagane.',
    'name is required' => 'Nazwa jest wymagana',
    'years' => [
        'one' => 'LAT DOŚWIADCZENIA',
        'firsttwo' => 'PONAD',
        'secondtwo' => '118 TYŚ.',
        'two' => 'ZADOWOLONYCH KLIENTÓW',
        'tree' => 'EVENTÓW ROCZNIE',
    ],
    'morepicture' => [
        'one' => 'Zobacz więcej zdjęć',
        'two' => 'Zwiń galerię',
    ],
    'menu' => [
        'gallery' => 'Galeria',
        'theytrustedus' => 'Zaufali nam',
        'contact' => 'Kontakt',
        'aboutus' => 'O nas',
    ],
    'download' => [
        'button' => 'Pobierz ofertę',
    ],
    'contact' => [
        'button' => 'SKONTAKTUJ SIĘ Z NAMI',
    ],
    'newsletter' => [
        'button' => 'Zapisz',
    ],
];
