<?php

return [
    'page created' => 'Strona poprawnie utworzona.',
    'page not found' => 'Strona nie znaleziona.',
    'page updated' => 'Strona poprawnie zaktualizowana.',
    'page deleted' => 'Strona poprawnie usunięta.',

    'offer created' => 'Oferta poprawnie utworzona.',
    'offer not found' => 'Oferta nie znaleziona.',
    'offer updated' => 'Oferta poprawnie zaktualizowana.',
    'offer deleted' => 'Oferta poprawnie usunięta.',

    'worker created' => 'Pracownik poprawnie utworzony.',
    'worker not found' => 'Pracownik nie znaleziony.',
    'worker updated' => 'Pracownik poprawnie zaktualizowany.',
    'worker deleted' => 'Pracownik poprawnie usunięty.',

    'template is required' => 'Nazwa szablonu jest wymagana.',
    'title is required' => 'Tytuł jest wymagany.',
    'body is required' => 'Treść jest wymagana.',
    'only one homepage allowed' => 'Tylko jedna strona główna jest dozwolona',
];
