<?php

return [
    'title' => [
        'pages' => 'Pages',
        'create page' => 'Create a page',
        'create worker' => 'Create a employee',
        'create offer' => 'Create a offer',
        'edit page' => 'Edit a page',
        'worker' => 'Employees',
        'workers' => 'Employees',
        'worker edit' => 'Employees edit',
        'offer' => 'Offer',
        'offer edit' => 'Offer edit',
        'offers' => 'Oferty'
    ],
    'button' => [
        'create page' => 'Create a page',
        'create worker' => 'Create employee',
        'create offer' => 'Create offer',
    ],
    'table' => [
        'name' => 'Name',
        'slug' => 'Slug',
        'body' => 'Body',
        'content' => 'Content'
    ],
    'form' => [
        'title' => 'Title',
        'slug' => 'Slug',
        'meta_data' => 'Meta data',
        'meta_title' => 'Meta title',
        'meta_description' => 'Meta description',
        'facebook_data' => 'Facebook data',
        'og_title' => 'Facebook title',
        'og_description' => 'Facebook description',
        'og_type' => 'Facebook type',
        'template' => 'Page template name',
        'is homepage' => 'Homepage ?',
        'body' => 'Body',
        'content' => 'Content',
        'name' => 'Name'
    ],
    'validation' => [
        'attributes' => [
            'title' => 'title',
            'body' => 'body',
        ],
    ],
    'facebook-types' => [
        'website' => 'Website',
        'product' => 'Product',
        'article' => 'Article',
    ],
    'navigation' => [
        'back to index' => 'Go back to the pages index',
    ],
    'list resource' => 'List pages',
    'create resource' => 'Create pages',
    'edit resource' => 'Edit pages',
    'destroy resource' => 'Delete pages',
    'offername is required' => 'The offer name is required.',
    'offerbody is required' => 'The offer body is required.',
    'name is required' => 'Name is required',
    'years' => [
        'one' => 'YEARS OF EXPERIENCE',
        'firsttwo' => 'OVER',
        'secondtwo' => '118 THOUSAND',
        'two' => 'SATISFIED CUSTOMERS',
        'tree' => 'EVENTS YEARLY',
    ],
    'morepicture' => [
        'one' => 'Load more picture',
        'two' => 'Hide gallery',
    ],
    'menu' => [
        'gallery' => 'Gallery',
        'theytrustedus' => 'They trusted us',
        'contact' => 'Contact',
        'aboutus' => 'About us',
    ],
    'download' => [
        'button' => 'Download offer',
    ],
    'contact' => [
        'button' => 'CONTACT US',
    ],
    'newsletter' => [
        'button' => 'Save',
    ],
];
