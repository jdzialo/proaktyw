<?php

return [
    'page created' => 'Page successfully created.',
    'page not found' => 'Page not found.',
    'page updated' => 'Page successfully updated.',
    'page deleted' => 'Page successfully deleted.',


    'offer created' => 'Offer successfully created.',
    'offer not found' => 'Offer not found.',
    'offer updated' => 'Offer successfully updated.',
    'offer deleted' => 'Offer successfully deleted.',

    'worker created' => 'Employee successfully created.',
    'worker not found' => 'Employee not found.',
    'worker updated' => 'Employee successfully updated.',
    'worker deleted' => 'Employee successfully deleted.',

    'template is required' => 'The template name is required.',
    'title is required' => 'The title is required.',
    'body is required' => 'The body is required.',
    'only one homepage allowed' => 'Only one homepage is allowed',
];
