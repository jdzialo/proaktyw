﻿<?php

return [
    'title' => [
        'main_sidebar' => 'Newsletter',
        'emails' => 'Emaile',
        'subscribers' => 'Subskrybenci',
        'groups' => 'Grupy',
        'edit subscriber' => 'Edytuj subsrybenta',
        'create subscriber' => 'Utwórz subsrybenta',
        'create group' => 'Utwórz grupę',
        'edit group' => 'Edytuj grupę',
    ],
    
    'table' => [
        'name' => 'Nazwa',
        'sended at' => 'Wysłany',
        'email' => 'E-mail',
        'groups' => 'Grupy',
    ],
    
    'button' => [
        'create email' => 'Utwórz e-mail',
        'edit email' => 'Edytuj e-mail',
        'create subscriber' => 'Utwórz subskrybenta',
        'create group' => 'Utórz grupę',
    ],
    'form' => [
        'groups' => 'Grupy',
        'title' => 'Tytuł',
        'body' => 'Kontent',
        'name' => 'Nazwa',
        'first_name' => 'Imię',
        'last_name' => 'Nazwisko',
        'email' => 'E-mail',
    ],
    
    'confirm_subscribtion' => 'Potwierdzenie subskrybcji',
    'unsubscribed' => 'Rezygnacja z newslettera zakończona pomyślnie',
    'subscribed' => 'Na email adres zostało wysłane potwierdzenie subskrybcji',
    'subscribed_email' => 'Dziękujemy za subskrybcję',
    'congratulations' => 'Pozdrawiamy',
    'already_subscribed' => 'Już jesteś podpisany na nasz newsletter',
    'hi' => 'Witaj',
    'thanks_for_subscribtion' => 'Dziękujemy za subskrypcję newslettera Podkarpackie Travel',
    'link_to_unsubscribe' => 'Z subskrypcji newslettera można zrezygnować klikając w link',
    'btn_unsubscribe' => 'Odpisuję się',
    'please_confirm_subscription' => 'Prosimy o potwierdzenie zamówienia bezpłatnej subskrypcji newslettera Podkarpackie Travel',
    'unsubscribe_possibility' => 'Z subskrypcji newslettera można zrezygnować w dowolnym momencie',
    'link_to_subscribe' => 'Aby potwierdzić zamówienie prosimy kliknąć na ten link',
    'i_subscribe' => 'Subskrybuję',
];
