<?php

return [
    'list resource' => 'Lista subskrybentów',
    'create resource' => 'Utwórz subskrybenta',
    'edit resource' => 'Edytuj subskrybenta',
    'destroy resource' => 'Usuń subskrybenta',
    'title' => [
        'subscribers' => 'Subskrybent',
        'create subscriber' => 'Utwórz subskrybenta',
        'edit subscriber' => 'Edytuj subskrybenta',
    ],
    'button' => [
        'create subscriber' => 'Utwórz subskrybenta',
    ],
    'table' => [
      'email' => 'Email',
      'name' => 'Nazwa',
      'groups' => 'Grupy',
    ],
    'form' => [
      'email' => 'Email',
      'first_name' => 'Imię',
      'last_name' => 'Nazwisko',
      'groups' => 'Grupy'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
