<?php

return [
    'title' => [
        'slider' => 'Slider',
        'create slider' => 'Nowy Slider',
        'edit slider' => 'Edycja Slidera'
    ],
    'popup' => [
        'slide_content' => 'Opis slajdu',
        'title' => 'Tytuł',
        'content' => 'Opis',
        'url' => 'Link',
    ],
    'button' => [
        'create slider' => 'Nowy Slider' 
    ],
    'form' => [
        'title' => "Tytuł"
    ] ,
    
];