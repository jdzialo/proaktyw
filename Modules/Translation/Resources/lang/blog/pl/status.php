<?php

return [
    'draft' => 'Szkic',
    'pending review' => 'Czeka na publikacje',
    'published' => 'Opublikowany',
    'unpublished' => 'Nieopublikowany',
];
