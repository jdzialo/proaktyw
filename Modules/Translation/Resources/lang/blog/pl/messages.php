<?php

return [
    /* Post management */
    'post created' => 'Utworzono wpis',
    'post not found' => 'Nie znaleziono wpisu',
    'post updated' => 'Wpis zaktualiozwany',
    'post deleted' => 'Wpis usunięty.',

    'title is required' => 'Tytuł jest wymagany',
    'slug is required' => 'Fragment url jest wymagany',
    'slug is unique' => 'Fragment url musi być unikalny',

    /* Category management */
    'category created' => 'Utworzono kategorie.',
    'category not found' => 'Nie znaleziono kategorii.',
    'category updated' => 'Kategoria zaktualiozwana',
    'category deleted' => 'Kategoria usunięta.',
];
