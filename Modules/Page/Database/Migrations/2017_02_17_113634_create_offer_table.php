<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('page__offers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
       
            $table->timestamps();
        });

        Schema::create('page__offers_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('offer_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name');
            $table->text('body');
            $table->text('content');
            $table->unique(['offer_id', 'locale']);
            $table->foreign('offer_id')->references('id')->on('page__offers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page__offers_translations');
        Schema::drop('page__offers');
    }
}
