<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->bind('page', function ($id) {
    return app(\Modules\Page\Repositories\PageRepository::class)->find($id);
});

$router->group(['prefix' => '/page'], function (Router $router) {
    $router->get('pages', [
        'as' => 'admin.page.page.index',
        'uses' => 'PageController@index',
        'middleware' => 'can:page.pages.index',
        ]);

    $router->get('pages/worker', [
        'as' => 'workerindex',
        'uses' => 'PageController@workerindex',
     //   'middleware' => 'can:page.pages.index',
        ]);

      $router->get('pages/offer', [
        'as' => 'offerindex',
        'uses' => 'PageController@offerindex',
     //   'middleware' => 'can:page.pages.index',
        ]);

    $router->get('pages/workercreate', [
        'as' => 'workercreate',
        'uses' => 'PageController@workercreate',
       // 'middleware' => 'can:page.pages.index',
        ]);

     $router->get('pages/offercreate', [
        'as' => 'offercreate',
        'uses' => 'PageController@offercreate',
       // 'middleware' => 'can:page.pages.index',
        ]);

    $router->post('pages/workerstore', [
        'as' => 'workerstore',
        'uses' => 'PageController@workerstore',
       // 'middleware' => 'can:page.pages.create',
        ]);

    $router->post('pages/offerstore', [
        'as' => 'offerstore',
        'uses' => 'PageController@offerstore',
       // 'middleware' => 'can:page.pages.create',
        ]);

    $router->get('pages/create', [
        'as' => 'admin.page.page.create',
        'uses' => 'PageController@create',
        'middleware' => 'can:page.pages.create',
        ]);
    $router->post('pages', [
        'as' => 'admin.page.page.store',
        'uses' => 'PageController@store',
        'middleware' => 'can:page.pages.create',
        ]);
    $router->get('pages/{page}/edit', [
        'as' => 'admin.page.page.edit',
        'uses' => 'PageController@edit',
        'middleware' => 'can:page.pages.edit',
        ]);
    $router->get('pages/{worker}/workeredit', [
        'as' => 'workeredit',
        'uses' => 'PageController@workeredit',
       // 'middleware' => 'can:page.pages.edit',
        ]);
     $router->get('pages/{offer}/offeredit', [
        'as' => 'offeredit',
        'uses' => 'PageController@offeredit',
       // 'middleware' => 'can:page.pages.edit',
        ]);

    $router->put('pages/{page}/edit', [
        'as' => 'admin.page.page.update',
        'uses' => 'PageController@update',
        'middleware' => 'can:page.pages.edit',
        ]);
    $router->put('pages/{worker}/workerupdate', [
        'as' => 'workerupdate',
        'uses' => 'PageController@workerupdate',
     //   'middleware' => 'can:page.pages.edit',
        ]);
     $router->put('pages/{offer}/offerupdate', [
        'as' => 'offerupdate',
        'uses' => 'PageController@offerupdate',
     //   'middleware' => 'can:page.pages.edit',
        ]);

    $router->delete('pages/{page}', [
        'as' => 'admin.page.page.destroy',
        'uses' => 'PageController@destroy',
        'middleware' => 'can:page.pages.destroy',
        ]);


     $router->delete('pages/{worker}/workerdestroy/', [
        'as' => 'workerdestroy',
        'uses' => 'PageController@workerdestroy',
       // 'middleware' => 'can:page.pages.destroy',
        ]);

     $router->delete('pages/{offer}/offerdestroy/', [
        'as' => 'offerdestroy',
        'uses' => 'PageController@offerdestroy',
       // 'middleware' => 'can:page.pages.destroy',
        ]);

    //offerindex
});





