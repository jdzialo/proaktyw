<?php

namespace Modules\Page\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Page\Entities\Page;
use Modules\Page\Entities\Worker;
use Modules\Page\Entities\Offer;

use Modules\Page\Http\Requests\CreatePageRequest;
use Modules\Page\Http\Requests\UpdatePageRequest;
use Modules\Page\Http\Requests\CreateOfferRequest;
use Modules\Page\Http\Requests\UpdateOfferRequest;
use Modules\Page\Http\Requests\CreateWorkerRequest;
use Modules\Page\Http\Requests\UpdateWorkerRequest;
use Modules\Page\Repositories\PageRepository;
use Modules\Page\Repositories\WorkerRepository;
use Modules\Page\Repositories\OfferRepository;
use Modules\Media\Repositories\FileRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class PageController extends AdminBaseController
{
    /**
     * @var PageRepository
     */
    private $page;


     /**
     * @var WorkerRepository
     */
     private $worker;


      /**
     * @var OfferRepository
     */
     private $offer;

     /**
     * @var FileRepository
     */
     private $file;

     public function __construct(PageRepository $page, FileRepository $file , OfferRepository $offer , WorkerRepository $worker)
     {
        parent::__construct();

        $this->page = $page;
        $this->file = $file;
        $this->worker = $worker;
        $this->offer = $offer;
        $this->assetPipeline->requireCss('icheck.blue.css');
    }

    public function index()
    {
        $pages = $this->page->all();

        return view('page::admin.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->assetPipeline->requireJs('ckeditor.js');

        return view('page::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePageRequest $request
     * @return Response
     */
    public function store(CreatePageRequest $request)
    {
        $this->page->create($request->all());

        return redirect()->route('admin.page.page.index')
        ->withSuccess(trans('page::messages.page created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Page $page
     * @return Response
     */
    public function edit(Page $page)
    {
        $gallery= $this->file->findFileByZoneForEntity('gallery', $page);
        $this->assetPipeline->requireJs('ckeditor.js');

        return view('page::admin.edit', compact('page', 'gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Page $page
     * @param  UpdatePageRequest $request
     * @return Response
     */
    public function update(Page $page, UpdatePageRequest $request)
    {
        $this->page->update($page, $request->all());

        if ($request->get('button') === 'index') {
            return redirect()->route('admin.page.page.index')
            ->withSuccess(trans('page::messages.page updated'));
        }

        return redirect()->back()
        ->withSuccess(trans('page::messages.page updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Page $page
     * @return Response
     */
    public function destroy(Page $page)
    {
        $this->page->destroy($page);

        return redirect()->route('admin.page.page.index')
        ->withSuccess(trans('page::messages.page deleted'));
    }


    public function workerindex()
    {

        $workers = $this->worker->all();


        return view('page::admin.worker.index', compact('workers') );
    }

    public function workercreate()

    {

        $this->assetPipeline->requireJs('ckeditor.js');

        return view('page::admin.worker.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateWorkerRequest $request
     * @return Response
     */
    public function workerstore(CreateWorkerRequest $request)

    {       
        $this->worker->create($request->all());

        return redirect()->route('workerindex')
        ->withSuccess(trans('page::messages.worker created'));

    }

    public function workeredit(Worker $worker)
    {

        $gallery= $this->file->findFileByZoneForEntity('worker', $worker);
      
        $this->assetPipeline->requireJs('ckeditor.js');

        return view('page::admin.worker.edit', compact('worker', 'gallery'));
    }



    public function workerupdate(Worker $worker, UpdateWorkerRequest $request)
    {
        $this->worker->update($worker, $request->all());

        if ($request->get('button') === 'index') {
            return redirect()->route('workerindex')
            ->withSuccess(trans('page::messages.worker updated'));
        }

        return redirect()->back()
        ->withSuccess(trans('page::messages.worker updated'));
    }


    public function workerdestroy(Worker $worker)
    {
        $this->worker->destroy($worker);

        return redirect()->route('workerindex')
        ->withSuccess(trans('page::messages.worker deleted'));
    }


     public function offerindex()
    {
        
         $offers = $this->offer->all();


        return view('page::admin.offer.index', compact('offers') );
    }
     public function offercreate()

    {

        $this->assetPipeline->requireJs('ckeditor.js');

        return view('page::admin.offer.create');

    }

      /**
     * Store a newly created resource in storage.
     *
     * @param  CreateWorkerRequest $request
     * @return Response
     */
    public function offerstore(CreateOfferRequest $request)

    {       

        $this->offer->create($request->all());

        return redirect()->route('offerindex')
        ->withSuccess(trans('page::messages.offer created'));

    }

    public function offeredit(Offer $offer)
    {

        $gallery= $this->file->findFileByZoneForEntity('offer', $offer);
      
        $this->assetPipeline->requireJs('ckeditor.js');

        return view('page::admin.offer.edit', compact('offer', 'gallery'));
    }



    public function offerupdate(Offer $offer, UpdateOfferRequest $request)
    {
        $this->offer->update($offer, $request->all());

        if ($request->get('button') === 'index') {
            return redirect()->route('offerindex')
            ->withSuccess(trans('page::messages.offer updated'));
        }

        return redirect()->back()
        ->withSuccess(trans('page::messages.offer updated'));
    }

        public function offerdestroy(Offer $offer)
    {
      
        $this->offer->destroy($offer);

        return redirect()->route('offerindex')
        ->withSuccess(trans('page::messages.offer deleted'));
    }

}
