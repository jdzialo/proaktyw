<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Entities\Worker;
use Modules\Page\Entities\Offer;
use Modules\Page\Repositories\PageRepository;
use Modules\Page\Http\Requests\CreateContactFormRequest;
use Modules\Slider\Repositories\SliderRepository;
use Modules\Page\Repositories\OfferRepository;
use Modules\Slider\Entities\Slider;
use Modules\Media\Image\Imagy;
use Illuminate\Support\Facades\App;

class PublicController extends BasePublicController
{
    /**
     * @var PageRepository
     */
    private $page;

    /**
     * @var Application
     */
    private $app;
    
    private $images;
    
    private $pages;

    private $workers;
    
    private $offer; 

      /**
     * @var SliderRepository
     */
      private $slider;

     /**
     * @var Imagy
     */
     private $imagy;

     public function __construct(PageRepository $page, Application $app, SliderRepository $slider , Imagy $imagy , OfferRepository $offer)
     {
        parent::__construct();
        $this->page = $page;
        $this->app = $app;
        $this->slider = $slider;
        $this->imagy = $imagy;
        $this->offer = $offer;
    }

    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function uri($slug)
    {

        $page = $this->findPageForSlug($slug);

        $this->throw404IfNotFound($page);

        $template = $this->getTemplateForPage($page);

        return view($template, compact('page'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function homepage()
    {

        // $date = new \DateTime();

        // $date->setDate(2001, 2, 28);
        // checkdate(2, 28, 2001);
        // echo $date->format('Y-m-d') . "\n";

        // $date->setDate(2001, 2, 29);
        // echo $date->format('Y-m-d') . "\n";

        // $date->setDate(2001, 14, 3);
        // echo $date->format('Y-m-d') . "\n";
        // $time = $date->setTime(16, 00);    
        // $current_time = date("Y-m-d")."T".$time->format("H:i:s")."Z"; 
        // echo $current_time;

        // echo "<br />";
        // $date = new \DateTime('2017-02-28');
        // $date->modify('+12 day');   
        // echo $date->format('Y-m-d');
        // die;
        $page = $this->page->findHomepage();  
        $this->pages = $page;
        $this->images = $this->getImagesByPage($page);
        $data = $this->loadData();
        
        $this->throw404IfNotFound($page);

        $template = $this->getTemplateForPage($page);

        return view($template,['gallery'=>$this->images, 'data' => $data])->with($data);
    }

    /**
     * Find a page for the given slug.
     * The slug can be a 'composed' slug via the Menu
     * @param string $slug
     * @return Page
     */
    private function findPageForSlug($slug)
    {
        $menuItem = app(MenuItemRepository::class)->findByUriInLanguage($slug, locale());

        if ($menuItem) {
            return $this->page->find($menuItem->page_id);
        }

        return $this->page->findBySlug($slug);
    }

    /**
     * Return the template for the given page
     * or the default template if none found
     * @param $page
     * @return string
     */
    private function getTemplateForPage($page)
    {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    /**
     * Throw a 404 error page if the given page is not found
     * @param $page
     */
    private function throw404IfNotFound($page)
    {
        if (is_null($page)) {
            $this->app->abort('404');
        }
    }


    private function getImagesByPage($page) {

        if (!is_null($page->files())) {
            $images = $page->files()->get();

            return $images;
        }
        return null;
    }
    

    public function mainSlider() {
        $slider = null;
        //$slides = $slider = Slider::with(['slides', 'slides.translations', 'slides.file'])->orderBy('created_at', 'desc')->first()->get()[0]->slides()->get();
        $files = Slider::with(['slides', 'slides.translations', 'slides.file'])->first()->files()->get();

        return $files;
    }

    public function loadData() {
        $data = [];
        $data['page'] = $this->pages;
        $default = $this->getSliderBySlug('defaultslider');
        $company = $this->getSliderBySlug('company');
        $references = $this->getSliderBySlug('files');
        if(!is_null($default))
        {
            $data['defaultslider'] = $default;
        }
        if(!is_null($company))
        {

            $data['company'] = $company;
        }
        if(!is_null($references))
        {
            $data['files'] = $references;
        }
        if ($this->images != null) {
            $data['images'] = $this->images;
        }
        $slider = $this->mainSlider();        
        if (!is_null($slider)) {
            $data['slider'] = $slider;
            $data['imagy'] = $this->imagy;
        }     

        $workers = $this->getWorkers();
        if(!is_null($workers))
        {
          $data['workers'] = $workers;
      }
      $offers = $this->getOffers();
      if(!is_null($offers))
      {
        $data['offers'] = $offers;
    }

    return $data;
}

public function getSliderBySlug($slug)
{
 $slider = $this->slider->findBySlugInLocale($slug, $this->app->getLocale());
 if($slider == null)
 {
    return null;
}
return  $this->slider->findBySlugInLocale($slug, $this->app->getLocale())->files()->get();
}

public function getWorkers()
{
    $workers = Worker::paginate(5);

    return $workers;
}


public function getOffers()
{       
   $offers =  $this->offer->allTranslatedIn(App::getLocale())->take(5);
   return $offers;
}


public function contactpage() {
    try {

        return view('contact.index');
    } catch (\Exception $e) {

        return redirect()->route("homepage")
        ->withErrors("Wystąpił błąd" + $e->getMessage());
    }
}


public function storecontact(CreateContactFormRequest $request) {

    $reqestAll = $request->all();
    
    $this->email = $reqestAll['email'];

    \Mail::send('contact.email', $reqestAll, function($message) {
        $message->from($this->email);
        $message->to(env('MAIL_USERNAME'), 'Admin')->subject(trans('page::contact.mail.subject'));
    });

    return \Redirect::route('page', '/')
    ->with('message', trans('page::contact.contact.send contactform'));
}




}
