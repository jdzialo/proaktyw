<?php

use Illuminate\Routing\Router;

/** @var Router $router */
if (! App::runningInConsole()) {
    $router->get('/', [
        'uses' => 'PublicController@homepage',
        'as' => 'homepage',
//        'middleware' => 'can:dashboard.index',
    ]);

    
       $router->get("kontakt", [
        'uses' => 'PublicController@contactpage',
        'as' => 'contact',
        'middleware' => 'can:dashboard.index',
    ]);
    $router->post('kontakt/store', [
        'uses' => 'PublicController@storecontact',
        'as' => 'contactStore',
        'middleware' => 'can:dashboard.index',
    ]);
    $router->any('{uri}', [
        'uses' => 'PublicController@uri',
        'as' => 'page',
        'middleware' => 'can:dashboard.index',
    ])->where('uri', '.*');
}
