<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateOfferRequest extends BaseFormRequest
{
    protected $translationsAttributesKey = 'page::offer.validation.attributes';
    
    public function rules()
    {

        return [

      //  'name' => 'required'
        
        
        ];
    }


    public function translationRules()
    {
        return [
        'name' => 'required',
         'body' => 'required'

        ];
    }

    public function authorize()
    {
        return true;
    } 


      public function messages()
    {
        return [
        //'name.required' => trans('page::contact.name is required'),

        ];
    }

    public function translationMessages()
    {
        return [
        'name.required' => trans('page::pages.offername is required'),
        'body.required' => trans('page::pages.offerbody is required')
        ];
    }


}
