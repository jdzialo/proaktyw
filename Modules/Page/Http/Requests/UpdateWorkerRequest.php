<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateWorkerRequest extends BaseFormRequest
{
    public function rules()
    {

        return [

        'name' => 'required',
        
        
        ];
    }

     public function translationRules()
    {
        return [
        //'name' => 'required',
        // 'body' => 'required'

        ];
    }

    public function authorize()
    {
        return true;
    } 

    public function messages()
    {
        return [
        'name.required' => trans('page::pages.name is required'),

        ];
    }


     public function translationMessages()
    {
        return [
      //  'name.required' => trans('page::offer.name is required'),
      //  'body.required' => trans('page::offer.body is required')
        ];
    }

  
}
