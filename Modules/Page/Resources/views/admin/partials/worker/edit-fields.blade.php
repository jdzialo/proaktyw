


<div class="box-body">
    <div class="box-body">
        <div class='form-group{{ $errors->has("name") ? ' has-error' : '' }}'>
            {!! Form::label("{name", trans('page::pages.table.name')) !!}
            {!! Form::text("name", $name, ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('page::pages.table.name')]) !!}
            {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
        </div>
       <div class='form-group{{ $errors->has("content") ? ' has-error' : '' }}'>
            {!! Form::label("{content", trans('page::pages.table.content')) !!}
            {!! Form::textarea("content", $content, ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('page::pages.table.content')]) !!}
            {!! $errors->first("content", '<span class="help-block">:message</span>') !!}
        </div>
     
    </div>
  
</div>
