<div class="box-body">
    <div class="box-body">
        
      <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[name]", trans('page::pages.form.name')) !!}
        <?php $old = $offer->hasTranslation($lang) ? $offer->translate($lang)->name : '' ?>
        {!! Form::text("{$lang}[name]", old("{$lang}.name", $old), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('page::pages.form.name')]) !!}
        {!! $errors->first("{$lang}.name", '<span class="help-block">:message</span>') !!}
    </div>


    <div class='{{ $errors->has("{$lang}.body") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[body]", trans('page::pages.form.body')) !!}
        <?php $old = $offer->hasTranslation($lang) ? $offer->translate($lang)->body : '' ?>
        <textarea class="ckeditor" name="{{$lang}}[body]" rows="10" cols="80">
            {!! old("$lang.body", $old) !!}
        </textarea>
        {!! $errors->first("{$lang}.body", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='{{ $errors->has("{$lang}.content") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[content]", trans('page::pages.form.content')) !!}
        <?php $old = $offer->hasTranslation($lang) ? $offer->translate($lang)->content : '' ?>
        <textarea class="ckeditor" name="{{$lang}}[content]" rows="10" cols="80">
            {!! old("$lang.content", $old) !!}
        </textarea>
        {!! $errors->first("{$lang}.content", '<span class="help-block">:message</span>') !!}
    </div>


    <?php if (config('asgard.page.config.partials.translatable.edit') !== []): ?>
        <?php foreach (config('asgard.page.config.partials.translatable.edit') as $partial): ?>
            @include($partial)
        <?php endforeach; ?>
    <?php endif; ?>
</div>
</div>