<div class="box-body">
    <div class="box-body">
         <div class='form-group{{ $errors->has("{$lang}.name") ? ' has-error' : '' }}'>
            {!! Form::label("{$lang}[name]", trans('page::pages.form.name')) !!}
            {!! Form::text("{$lang}[name]", old("{$lang}.name"), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('page::pages.form.name')]) !!}
            {!! $errors->first("{$lang}.name", '<span class="help-block">:message</span>') !!}
        </div>


        <div class='{{ $errors->has("{$lang}.body") ? ' has-error' : '' }}'>
            {!! Form::label("{$lang}[body]", trans('page::pages.form.body')) !!}
            <textarea class="ckeditor" name="{{$lang}}[body]" rows="10" cols="80">{{ old("{$lang}.body") }}</textarea>
            {!! $errors->first("{$lang}.body", '<span class="help-block">:message</span>') !!}
        </div>

        <div class='{{ $errors->has("{$lang}.content") ? ' has-error' : '' }}'>
            {!! Form::label("{$lang}[content]", trans('page::pages.form.content')) !!}
            <textarea class="ckeditor" name="{{$lang}}[content]" rows="10" cols="80">{{ old("{$lang}.content") }}</textarea>
            {!! $errors->first("{$lang}.content", '<span class="help-block">:message</span>') !!}
        </div>


        <?php if (config('asgard.page.config.partials.translatable.create') !== []): ?>
            <?php foreach (config('asgard.page.config.partials.translatable.create') as $partial): ?>
                @include($partial)
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
 
</div>
