<?php

namespace Modules\Page\Events;

use Modules\Media\Contracts\StoringMedia;
use Modules\Page\Entities\Offer;

class OfferWasUpdated implements StoringMedia {

    /**
     * @var array
     */
    public $data;

    /**
     * @var int
     */
    public $offerId;

    /**
     * @var offer
     */
    public $offer;

    public function __construct($offerId, array $data, $offer) {
        $this->data = $data;

        $this->offerId = $offerId;
        $this->offer = $offer;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData() {
        return $this->data;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity() {
        return $this->offer;
    }

}
