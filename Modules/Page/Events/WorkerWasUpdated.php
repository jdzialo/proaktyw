<?php

namespace Modules\Page\Events;

use Modules\Media\Contracts\StoringMedia;
use Modules\Page\Entities\Worker;

class WorkerWasUpdated implements StoringMedia {

    /**
     * @var array
     */
    public $data;

    /**
     * @var int
     */
    public $workerId;

    /**
     * @var worker
     */
    public $worker;

    public function __construct($workerId, array $data, $worker) {
        $this->data = $data;

        $this->workerId = $workerId;
        $this->worker = $worker;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData() {
        return $this->data;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity() {
        return $this->worker;
    }

}
