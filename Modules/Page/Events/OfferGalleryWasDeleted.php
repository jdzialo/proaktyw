<?php

namespace Modules\Page\Events;

use Modules\Media\Contracts\DeletingMedia;

class OfferGalleryWasDeleted implements DeletingMedia {

    /**
     * @var string
     */
    private $offerClass;

    /**
     * @var int
     */
    private $offerId;

    /**
     * @var object
     */
    public $offer;

    public function __construct($offer) {
        $this->$offer = $offer;
        $this->offerId = $offer->id;
        $this->postClass = get_class($offer);
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId() {
        return $this->offerId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName() {
        return $this->offerClass;
    }

}