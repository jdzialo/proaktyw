<?php

namespace Modules\Page\Events;

use Modules\Media\Contracts\DeletingMedia;

class WorkerWasDeleted implements DeletingMedia {

    /**
     * @var string
     */
    private $workerClass;

    /**
     * @var int
     */
    private $workerId;

    /**
     * @var object
     */
    public $worker;

    public function __construct($worker) {
        $this->$worker = $worker;
        $this->workerId = $worker->id;
        $this->postClass = get_class($worker);
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId() {
        return $this->workerId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName() {
        return $this->workerClass;
    }

}