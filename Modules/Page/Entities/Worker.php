<?php

namespace Modules\Page\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Tag\Contracts\TaggableInterface;
use Modules\Tag\Traits\TaggableTrait;

class Worker extends Model
{
  
       
      use  TaggableTrait,
        NamespacedEntity,
        \Modules\Media\Support\Traits\MediaRelation;

    protected $table = 'page__workers';
   
    protected $fillable = [
    
        'id',
        'name',
        'content'
  
    ];

    
}
