<?php

namespace Modules\Page\Entities;

use Illuminate\Database\Eloquent\Model;

class OfferTranslation extends Model
{
    protected $table = 'page__offers_translations';
    protected $fillable = [
        'id',
        'offer_id',
        'body',
        'name',
        'content'
 
    ];
}
