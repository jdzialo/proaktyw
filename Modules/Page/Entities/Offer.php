<?php

namespace Modules\Page\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Tag\Contracts\TaggableInterface;
use Modules\Tag\Traits\TaggableTrait;

class Offer extends Model   {

    use Translatable,
        TaggableTrait,
        NamespacedEntity,
        \Modules\Media\Support\Traits\MediaRelation;

    protected $table = 'page__offers';
    public $translatedAttributes = [
       
        'offer_id',
        'body',
        'name',
        'content'
       
       
    ];
    protected $fillable = [
   
            'name',
            'offer_id',
            'body',
            'content'
   
    ];
  
    protected static $entityNamespace = 'asgardcms/page';

  public function __call($method, $parameters) {
        #i: Convert array to dot notation
        $config = implode('.', ['asgard.page.config.relations', $method]);

        #i: Relation method resolver
        if (config()->has($config)) {
            $function = config()->get($config);

            return $function($this);
        }

        #i: No relation found, return the call to parent (Eloquent) to handle it.
        return parent::__call($method, $parameters);
    }
    
    /**
     * Deleting files connections (from media__imageables table)
     * by deleting a main entity
     */
    public function delete() {
        foreach ($this->files as $file) {
            $file->pivot->delete();
        }
        return parent::delete();
    }
    
}
