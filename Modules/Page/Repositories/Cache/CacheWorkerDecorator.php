<?php

namespace Modules\Page\Repositories\Cache;

use Modules\Core\Repositories\Cache\BaseCacheDecorator;
use Modules\Page\Repositories\PageRepository;
use Modules\Page\Repositories\WorkerRepository;

class CacheWorkerDecorator extends BaseCacheDecorator implements WorkerRepository
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(WorkerRepository $worker)
    {
        parent::__construct();
        $this->entityName = 'worker';
        $this->repository = $worker;
    }




 
}
