<?php

namespace Modules\Page\Repositories\Cache;

use Modules\Core\Repositories\Cache\BaseCacheDecorator;
use Modules\Page\Repositories\PageRepository;
use Modules\Page\Repositories\OfferRepository;

class CacheOfferDecorator extends BaseCacheDecorator implements OfferRepository
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(OfferRepository $offer)
    {
        parent::__construct();
        $this->entityName = 'offer';
        $this->repository = $offer;
    }




 
}
