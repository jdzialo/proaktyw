<?php

namespace Modules\Page\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

use Modules\Page\Repositories\WorkerRepository;
use Modules\Page\Events\WorkerWasCreated;
use Modules\Page\Events\WorkerWasDeleted;
use Modules\Page\Events\WorkerWasUpdated;

class EloquentWorkerRepository extends EloquentBaseRepository implements WorkerRepository
{
   
    public function create($data)
    {
      
        $worker = $this->model->create($data);

        event(new WorkerWasCreated($worker->id, $data, $worker));

       
        return $worker;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data)
    {
        
        $model->update($data);

        event(new WorkerWasUpdated($model->id, $data, $model));

       

        return $model;
    }

    public function destroy($worker)
    {
        

        event(new WorkerWasDeleted($worker));

        return $worker->delete();
    }

}
