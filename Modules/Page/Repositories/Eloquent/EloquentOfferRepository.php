<?php

namespace Modules\Page\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

use Modules\Page\Repositories\OfferRepository;
use Modules\Page\Events\OfferWasCreated;
use Modules\Page\Events\OfferWasDeleted;
use Modules\Page\Events\OfferWasUpdated;
use Modules\Page\Events\OfferGalleryWasCreated;
use Modules\Page\Events\OfferGalleryWasDeleted;
use Modules\Page\Events\OfferGalleryWasUpdated;

class EloquentOfferRepository extends EloquentBaseRepository implements OfferRepository
{
   
    public function create($data)
    {
      
        $offer = $this->model->create($data);

        event(new OfferWasCreated($offer->id, $data, $offer));
        event(new OfferGalleryWasCreated($offer->id, $data, $offer));
       
        return $offer;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data)
    {
        
        $model->update($data);

        event(new OfferWasUpdated($model->id, $data, $model));
        event(new OfferGalleryWasUpdated($model->id, $data, $model));
       

        return $model;
    }

    public function destroy($offer)
    {
        

        event(new OfferWasDeleted($offer));
        event(new OfferGalleryWasDeleted($offer));
        return $offer->delete();
    }

}
