<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriberGroupPivotTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('newsletter__subscriber_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscriber_id')->unsigned();
            $table->integer('group_id')->unsigned();
            $table->unique(['subscriber_id', 'group_id']);

            $table->timestamps();
        });
        Schema::table('newsletter__subscriber_group', function (Blueprint $table) {
            $table->foreign('subscriber_id')->references('id')->on('newsletter__subscribers')->onDelete('cascade');
            $table->foreign('group_id')->references('id')->on('newsletter__groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('newsletter__subscriber_group', function (Blueprint $table) {
            $table->dropForeign(['subscriber_id']);
            $table->dropForeign(['group_id']);
        });
        Schema::drop('newsletter__subscriber_group');
    }
}
