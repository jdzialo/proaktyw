<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <title>PROT</title>
        
        <?php
            $body = 'width: 100%;background-color: #fff;margin: 0;padding: 0;-webkit-font-smoothing: antialiased;font-family: Lato, Georgia, Times, serif;color: #000;';
            $bgGray = 'background-color: #f9f9f9;';
            
            
            $mailTitle = 'color: #fff;background: #86b829;padding: 15px 0;text-align: center;font-size: 20px;';
            $button = 'color: #fff;background: #f3a02c;padding: 15px 35px;text-align: center;margin: 0 auto;text-decoration: none;display: inline-block;';
            $style = array(
                'mailTitle' => $mailTitle,
                'button' => $button,
            );
        ?>
        
        <style type="text/css">
            @import url(http://fonts.googleapis.com/css?family=Lato:400,300,700,700italic,400italic,300italic);
            table {
                border-collapse: collapse;
            }
            .img-responsive {
                display: block;
                max-width: 100%;
                height: auto;
                margin: 0 auto;
            }
        </style>
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="<?= $body ?>">
        <tbody>
            <!-- Wrapper -->
            <table width="580" border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td width="100%" valign="top">
                        <!-- Start Header-->
                        <table border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" style="width: 580px;margin:0 auto;border-top: 5px solid #9ac118;">
                            <tr>
                                <td width="100%" bgcolor="#ffffff">
                                    <!-- Logo -->
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                        <tr>
                                            <td width="580px" style="padding: 25px 10px;text-align: center;" class="center">
                                                <a href="{{ url('/') }}">
                                                    <img src="{{ url('/') }}/assets/img/logo.png" class="img-responsive" border="0" />
                                                </a>
                                            </td>
                                        </tr>
                                    </table><!-- End Logo -->
                                </td>
                            </tr>
                        </table><!-- End Header -->
                        
                        @include($partial, ['style' => $style])

                        <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" style="border-top: 1px solid grey;">
                            <tr>
                                <td width="100%" bgcolor="#ffffff">
                                    <table width="100%" border="0" cellpadding="10" cellspacing="0" align="center" class="deviceWidth">
                                        <tr>
                                            <td style="padding: 15px;">
                                                {{ trans('newsletter::newsletter.congratulations') }}, <br />
                                                <a href="{{ url('/') }}" target="_blank">@setting('core::company-name')</a>
                                                <br />
                                                -----------------------------------------------------------------
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 15px;">
                                                @setting('core::company-name') <br />
                                                @setting('core::company-address'), @setting('core::company-zipcode'), @setting('core::company-city') <br />
                                                NIP: @setting('core::company-nip') <br />
                                                tel. @setting('core::company-phone')
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 15px;">
                                                e-mail: <a href="mailto:@setting('core::company-email')" target="_blank">@setting('core::company-email')</a> <br />
                                                <a href="{{ url('/') }}" target="_blank">{{ url('/') }}</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table> <!-- End Wrapper -->
            <div style="display:none; white-space:nowrap; font:15px courier; color:#ffffff;">
                - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            </div>
        </tbody>
    </body>
</html>