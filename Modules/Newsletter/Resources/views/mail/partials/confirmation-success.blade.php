<table width="580" border="0" cellpadding="0" cellspacing="0" align="center" style="border-top: 1px solid grey;">
    <tr>
        <td width="100%" bgcolor="#ffffff">
            <table width="100%" border="0" cellpadding="10" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td style="padding: 15px;">
                        {{ trans('newsletter::newsletter.hi') }}, {{ $subscriber->first_name }} {{ $subscriber->last_name }}<br />
                        {{ trans('newsletter::newsletter.thanks_for_subscribtion') }}!<br />
                        {{ trans('newsletter::newsletter.link_to_unsubscribe') }}:<br />
                        
                        <a href="{{ URL::route('newsletter.subscribtion.unconfirmation', $subscriber->hash) }}" style="{{ $style['button'] }}">{{ trans('newsletter::newsletter.btn_unsubscribe') }}</a><br />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>