<h1>{{ $email->title }}</h1>
<table width="580" border="0" cellpadding="0" cellspacing="0" align="center" style="border-top: 1px solid grey;">
    <tr>
        <td width="100%" bgcolor="#ffffff">
            <table width="100%" border="0" cellpadding="10" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td style="padding: 15px;">
                        {!! $email->body !!}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>