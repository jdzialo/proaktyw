<div class="box-body">
      <div class='form-group{{ $errors->has("name") ? ' has-error' : '' }}'>
          {!! Form::label("name", trans('newsletter::newsletter.form.name')) !!}
          {!! Form::text("name", old('name'), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('newsletter::newsletter.form.name')]) !!}
          {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
      </div>
</div>
