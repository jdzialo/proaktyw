<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.title") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[title]", trans('newsletter::newsletter.form.title')) !!}
        {!! Form::text("{$lang}[title]", $email->translate($lang)->title, ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('page::pages.form.title')]) !!}
        {!! $errors->first("{$lang}.title", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='{{ $errors->has("{$lang}.body") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[body]", trans('newsletter::newsletter.form.body')) !!}
        <textarea class="ckeditor" name="{{$lang}}[body]" rows="10" cols="80">{{ $email->translate($lang)->body }}</textarea>
        {!! $errors->first("{$lang}.body", '<span class="help-block">:message</span>') !!}
    </div>
</div>
