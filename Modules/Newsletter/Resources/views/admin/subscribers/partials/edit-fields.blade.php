<div class="box-body">
    <div class="box-body">
        <div class='form-group{{ $errors->has("email") ? ' has-error' : '' }}'>
            {!! Form::label("email", trans('newsletter::newsletter.form.email')) !!}
            {!! Form::email("email", $subscriber->email, ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('newsletter::newsletter.form.email')]) !!}
            {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
        </div>

        <div class='form-group{{ $errors->has("first_name") ? ' has-error' : '' }}'>
            {!! Form::label("first_name", trans('newsletter::newsletter.form.first_name')) !!}
            {!! Form::text("first_name",  $subscriber->first_name, ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('newsletter::newsletter.form.first_name')]) !!}
            {!! $errors->first("first_name", '<span class="help-block">:message</span>') !!}
        </div>

        <div class='form-group{{ $errors->has("last_name") ? ' has-error' : '' }}'>
            {!! Form::label("last_name", trans('newsletter::newsletter.form.last_name')) !!}
            {!! Form::text("last_name",  $subscriber->last_name, ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('newsletter::newsletter.form.last_name')]) !!}
            {!! $errors->first("last_name", '<span class="help-block">:message</span>') !!}
        </div>

      <div class="form-group">
        <label for="groups">{{ trans('newsletter::newsletter.form.groups') }}</label>
        <select name="groups[]" id="groups" class="input-tags selectized" multiple="multiple">
          @foreach ($groups as $group)
            <option value="{{ $group->id }}" @if($subscriber->groups->contains($group)) selected @endif>{{ $group->name }}</option>
          @endforeach
        </select>
      </div>

    </div>
</div>
