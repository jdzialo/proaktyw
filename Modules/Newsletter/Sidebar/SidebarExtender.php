<?php

namespace Modules\Newsletter\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\User\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('newsletter::newsletter.title.main_sidebar'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('newsletter::newsletter.title.emails'), function (Item $item) {
                    $item->icon('fa fa-envelope');
                    $item->weight(0);
                    $item->append('admin.newsletter.email.create');
                    $item->route('admin.newsletter.email.index');
                    $item->authorize(
                        $this->auth->hasAccess('newsletter.emails.index')
                    );
                });
                $item->item(trans('newsletter::newsletter.title.subscribers'), function (Item $item) {
                    $item->icon('fa fa-user');
                    $item->weight(0);
                    $item->append('admin.newsletter.subscriber.create');
                    $item->route('admin.newsletter.subscriber.index');
                    $item->authorize(
                        $this->auth->hasAccess('newsletter.subscribers.index')
                    );
                });
                $item->item(trans('newsletter::newsletter.title.groups'), function (Item $item) {
                    $item->icon('fa fa-object-group');
                    $item->weight(0);
                    $item->append('admin.newsletter.group.create');
                    $item->route('admin.newsletter.group.index');
                    $item->authorize(
                        $this->auth->hasAccess('newsletter.groups.index')
                    );
                });
// append



            });
        });

        return $menu;
    }
}
