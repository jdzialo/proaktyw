<?php

namespace Modules\Newsletter\Entities;

use Illuminate\Database\Eloquent\Model;

class EmailTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'body'];
    protected $table = 'newsletter__email_translations';
}
