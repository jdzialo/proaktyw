<?php

namespace Modules\Newsletter\Entities;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model {

    protected $table = 'newsletter__subscribers';
    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'status',
        'hash'
    ];

    public function groups() {
        return $this->belongsToMany('Modules\Newsletter\Entities\Group', 'newsletter__subscriber_group');
    }
    
    public static function generateHash(){
        return md5(date('d-m-Y-H-s'));
    }
	
    public static function checkSubscribing($email) {
        $subscriber = self::where('email', $email)->first();
        if (empty($subscriber) || $subscriber->status !== Status::ACTIVE) {
            return false;
        }
        return true;
    }
    
    public static function getSubscriber($email) {
        return self::where('email', $email)->first();
    }

}
