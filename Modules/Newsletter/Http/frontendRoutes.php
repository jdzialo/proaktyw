<?php

use Illuminate\Routing\Router;

$router->group(['prefix' => '/newsletter'], function (Router $router) {
    $router->post('/subscribe', [
        'as' => 'newsletter.subscribe.create',
        'uses' => 'PublicController@create'
    ]);
    $router->post('/subscribe/store', [
        'as' => 'newsletter.subscribe.store',
        'uses' => 'PublicController@store'
    ]);
    
    $router->get('/subscribtion-confirmation/{hash}', [
        'as' => 'newsletter.subscribtion.confirmation',
        'uses' => 'PublicController@confirmation'
    ]);
    
    $router->get('/subscribtion-unconfirmation/{hash}', [
        'as' => 'newsletter.subscribtion.unconfirmation',
        'uses' => 'PublicController@unconfirmation'
    ]);
});