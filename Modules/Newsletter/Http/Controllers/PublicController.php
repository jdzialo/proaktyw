<?php

namespace Modules\Newsletter\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Newsletter\Entities\Subscriber;
use Modules\Newsletter\Repositories\SubscriberRepository;
use Illuminate\Http\Request;
use App\Status;
use Modules\Newsletter\Mail\ConfirmNewsletter;
use Modules\Newsletter\Mail\ConfirmationSuccessNewsletter;
use Illuminate\Support\Facades\Mail;

class PublicController extends BasePublicController {

    /**
     * @var SubscriberRepository
     */
    private $subscriber;

    /**
     * @var Application
     */
    private $app;

    public function __construct(Subscriber $subscriber, Application $app) {
        parent::__construct();
        
        $this->subscriber = $subscriber;
        $this->app = $app;
    }

    public function create(Request $request) {
        $breadcrumbs = array(
            trans('newsletter::newsletter.confirm_subscribtion') => ''
            );
        
        return view('page.newsletter-subscribe', [
            'email' => $request->email,
            'breadcrumbs' => $breadcrumbs
            ]);
    }

    public function store(Request $request) {
     
        $subscriberAlreadyExist = Subscriber::where(['email' => $request->email])->get();

        if($subscriberAlreadyExist->isEmpty()){
            $subscriber = $this->subscriber->create(
                array_merge(
                    $request->all(), [
                        'status' => Status::ACTIVE // Set active status to new subscriber
                        ]
                        )
                );
            

            
            return \Redirect::route('homepage')
            ->with('message', trans('messages.newsletter.subscribed'));   

        }else{
            
           return \Redirect::route('homepage')
           ->with('message', trans('essages.newsletter.already_subscribed'));
       }
   }
   
   public function confirmation($hash) {
    if (empty($hash)) {
        return redirect()->route('homepage');
    }
    
    $subscriber = Subscriber::where('hash', $hash)->first();
    
    if (!$subscriber) {
        return redirect()->route('homepage')
        ->withSuccess(trans('newsletter::newsletter.already_subscribed'));
    }
    
    $subscriber->update([
        'status' => Status::ACTIVE
        ]);
    
    Mail::to($subscriber->email)->send(new ConfirmationSuccessNewsletter($subscriber));
    
    return redirect()->route('homepage')->withSuccess(trans('newsletter::newsletter.subscribed_email'));
}

public function unconfirmation($hash) {
    if (empty($hash)) {
        return redirect()->route('homepage');
    }
    
    $subscriber = Subscriber::where('hash', $hash)->first();
    
    if (!$subscriber) {
        return redirect()->route('homepage');
    }
    
    $subscriber->update([
        'status' => Status::INACTIVE
        ]);
    
    return redirect()->route('homepage')->withSuccess(trans('newsletter::newsletter.unsubscribed'));
}

}
