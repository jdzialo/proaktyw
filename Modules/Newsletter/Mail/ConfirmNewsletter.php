<?php

namespace Modules\Newsletter\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Newsletter\Entities\Subscriber;

class ConfirmNewsletter extends Mailable {

    use Queueable, SerializesModels;
    
    private $subscriber;
    
    /**
     * Create a new message instance.
     * @return void
     */
    public function __construct(Subscriber $subscriber) {
        $this->subscriber = $subscriber;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->view('newsletter::mail.template')->subject(trans('newsletter::newsletter.confirm_subscribtion'))->with([
            'subscriber' => $this->subscriber,
            'partial' => 'newsletter::mail.partials.confirmation'
        ]);
    }

}
