<?php

namespace Modules\Newsletter\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Modules\Newsletter\Entities\Email;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Newsletter extends Mailable implements ShouldQueue {

    use Queueable, SerializesModels;

    private $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Email $email) {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
//        return $this->view('newsletter::mail.newsletter', ['email' => $this->email]);
        
        return $this->view('newsletter::mail.template')->subject('Newsletter')->with([
            'email' => $this->email,
            'partial' => 'newsletter::mail.newsletter'
        ]);
        
    }

}
