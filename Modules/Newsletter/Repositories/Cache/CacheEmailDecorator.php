<?php

namespace Modules\Newsletter\Repositories\Cache;

use Modules\Newsletter\Repositories\EmailRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheEmailDecorator extends BaseCacheDecorator implements EmailRepository
{
    public function __construct(EmailRepository $email)
    {
        parent::__construct();
        $this->entityName = 'newsletter.emails';
        $this->repository = $email;
    }
}
