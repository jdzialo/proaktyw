<?php

namespace Modules\Category\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryItemRequest extends FormRequest {

    public function rules() {
        return [];
    }

    public function authorize() {
        return true;
    }

    public function messages() {
        return [];
    }

}
