<?php

namespace Modules\Category\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface CategoryItemRepository extends BaseRepository
{
    /**
     * Get online root elements
     *
     * @param  int    $categoryId
     * @return object
     */
    public function rootsForCategory($categoryId);

    /**
     * Get all root elements
     *
     * @param  int    $categoryId
     * @return object
     */
    public function allRootsForCategory($categoryId);

    /**
     * Get the category items ready for routes
     * @return mixed
     */
    public function getForRoutes();

    /**
     * Get the root category item for the given category id
     * @param  int    $categoryId
     * @return object
     */
    public function getRootForCategory($categoryId);

    /**
     * Return a complete tree for the given category id
     *
     * @param  int    $categoryId
     * @return object
     */
    public function getTreeForCategory($categoryId);

    /**
     * @param  string $uri
     * @param  string $locale
     * @return object
     */
    public function findByUriInLanguage($uri, $locale);
}
