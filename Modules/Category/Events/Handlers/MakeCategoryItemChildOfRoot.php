<?php

namespace Modules\Category\Events\Handlers;

use Modules\Category\Events\CategoryItemWasCreated;
use Modules\Category\Repositories\CategoryItemRepository;

class MakeCategoryItemChildOfRoot
{
    /**
     * @var CategoryItemRepository
     */
    private $categoryItem;

    public function __construct(CategoryItemRepository $categoryItem)
    {
        $this->categoryItem = $categoryItem;
    }

    public function handle(CategoryItemWasCreated $event)
    {
        $root = $this->categoryItem->getRootForCategory($event->categoryItem->category_id);

        if (! $this->isRoot($event->categoryItem)) {
            $event->categoryItem->makeChildOf($root);
        }
    }

    /**
     * Check if the given Category item is not already a root Category item
     * @param  object $categoryItem
     * @return bool
     */
    private function isRoot($categoryItem)
    {
        return (bool) $categoryItem->is_root;
    }
}
