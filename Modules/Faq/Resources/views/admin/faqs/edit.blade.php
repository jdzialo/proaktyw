@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('faq::faqs.title.edit faq') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.faq.faq.index') }}">{{ trans('faq::faqs.title.faqs') }}</a></li>
        <li class="active">Edycja pytania</li>
    </ol>
@stop

@section('styles')
    {!! Theme::script('js/vendor/ckeditor/ckeditor.js') !!}
@stop

@section('content')
    {!! Form::open(['route' => ['admin.faq.faq.update', $faq->id], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    
                    <div class='form-group{{ $errors->has('name') ? ' has-error' : '' }}'>
                        {!! Form::label('name', 'Nazwa') !!}
                        <input type="text" id="name" class="form-control" name="name" value="{{ $faq->name }}" />
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>
                    
                    <div class='form-group{{ $errors->has('email') ? ' has-error' : '' }}'>
                        {!! Form::label('email', 'E-mail') !!}
                        <input type="email" id="email" class="form-control" name="email" value="{{ $faq->email }}" />
                        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                    </div>
                    
                    <div class='form-group{{ $errors->has('title') ? ' has-error' : '' }}'>
                        {!! Form::label('title', 'Pytanie') !!}
                        <input type="text" id="title" class="form-control" name="title" value="{{ $faq->title }}" required />
                        {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                    </div>
                    
                    <div class='form-group{{ $errors->has('answer') ? ' has-error' : '' }}'>
                        {!! Form::label('answer', 'Odpowiedź') !!}
                        <textarea id="answer" class="form-control" name="answer">{{ $faq->answer }}</textarea>
                        {!! $errors->first('answer', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.faq.faq.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.faq.faq.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@stop
