<?php

return [
    'list resource' => 'List faqs',
    'create resource' => 'Create faqs',
    'edit resource' => 'Edit faqs',
    'destroy resource' => 'Destroy faqs',
    'title' => [
        'faqs' => 'Faq',
        'create faq' => 'Create a faq',
        'edit faq' => 'Edit a faq',
    ],
    'button' => [
        'create faq' => 'Create a faq',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
