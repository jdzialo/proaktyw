<?php

namespace Modules\Faq\Entities;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model {

    protected $table = 'faq__faqs';
    protected $fillable = [
        'name',
        'email',
        'title',
        'answer'
    ];

}