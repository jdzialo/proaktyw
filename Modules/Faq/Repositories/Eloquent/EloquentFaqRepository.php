<?php

namespace Modules\Faq\Repositories\Eloquent;

use Modules\Faq\Repositories\FaqRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentFaqRepository extends EloquentBaseRepository implements FaqRepository {

    /**
     * Count all records
     * @return int
     */
    public function countNotAnswered() {
        return $this->model->whereNull('answer')->count();
    }

}
