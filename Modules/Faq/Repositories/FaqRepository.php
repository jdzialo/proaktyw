<?php

namespace Modules\Faq\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface FaqRepository extends BaseRepository {
    
    /**
     * Count all records
     * @return int
     */
    public function countNotAnswered();
}
