<?php

namespace Modules\Faq\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\User\Contracts\Authentication;
use Modules\Faq\Repositories\FaqRepository;
use Maatwebsite\Sidebar\Badge;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender {

    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth) {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu) {
//         $menu->group(trans('core::sidebar.content'), function (Group $group) {
//             $group->item('FAQ', function (Item $item) {
//                 $item->icon('fa fa-commenting');
//                 $item->weight(10);
//                 $item->route('admin.faq.faq.index');
//                 $item->badge(function (Badge $badge, FaqRepository $faq) {
//                     $badge->setClass('bg-yellow');
//                     $badge->setValue($faq->countNotAnswered());
//                 });
// //                $item->item('Pytania użytkowników', function (Item $item) {
// //                    $item->icon('fa fa-bars');
// //                    $item->weight(0);
// //                    $item->append('admin.faq.faq.create');
// //                    $item->route('admin.faq.faq.index');
// //                    $item->authorize(
// //                        $this->auth->hasAccess('faq.faqs.index')
// //                    );
// //                });
//             });
//         });

        return $menu;
    }

}
