<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/faq'], function (Router $router) {
    $router->get('/pytania', [
        'as' => 'faq.index',
        'uses' => 'PublicController@index'
    ]);
    $router->post('', [
        'as' => 'faq.store',
        'uses' => 'PublicController@store'
    ]);
});