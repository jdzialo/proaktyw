<?php

namespace Modules\Faq\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Faq\Entities\Faq;
use Modules\Faq\Repositories\FaqRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class FaqController extends AdminBaseController {

    /**
     * @var FaqRepository
     */
    private $faq;

    public function __construct(FaqRepository $faq) {
        parent::__construct();

        $this->faq = $faq;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $faqs = $this->faq->all();

        return view('faq::admin.faqs.index', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('faq::admin.faqs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        $this->faq->create($request->all());

        return redirect()->route('admin.faq.faq.index')
                        ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('faq::faqs.title.faqs')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Faq $faq
     * @return Response
     */
    public function edit(Faq $faq) {
        return view('faq::admin.faqs.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Faq $faq
     * @param  Request $request
     * @return Response
     */
    public function update(Faq $faq, Request $request) {
        $this->faq->update($faq, $request->all());

        return redirect()->route('admin.faq.faq.index')
                        ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('faq::faqs.title.faqs')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Faq $faq
     * @return Response
     */
    public function destroy(Faq $faq) {
        $this->faq->destroy($faq);

        return redirect()->route('admin.faq.faq.index')
                        ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('faq::faqs.title.faqs')]));
    }

}
