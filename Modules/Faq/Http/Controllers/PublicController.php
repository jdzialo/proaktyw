<?php

namespace Modules\Faq\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Faq\Repositories\FaqRepository;
use Modules\Faq\Entities\Faq;
use Illuminate\Http\Request;
use Modules\Faq\Http\Requests\CreateFaqRequest;

class PublicController extends BasePublicController {


    /**
     * @var Application
     */
    private $app;

    public function __construct(Application $app, FaqRepository $faq) {
        parent::__construct();
        $this->product = $faq;
        $this->app = $app;
    }

    public function index() {
        $entities = Faq::whereNotNull('answer')->orderBy('created_at', 'DESC')->paginate(25);
        
        return view('faq.index', compact('entities'));
    }
    
    public function store(CreateFaqRequest $request) {
        Faq::create($request->all());

        return redirect()->route('faq.index')
                        ->withSuccess('Dziękujemy za przesłane pytanie. Wkrótce odpowiemy');
    }
    
}
