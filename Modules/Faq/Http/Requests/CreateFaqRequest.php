<?php

namespace Modules\Faq\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateFaqRequest extends BaseFormRequest {

    protected $translationsAttributesKey = 'faq::faqs.validation.attributes';

    public function rules() {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'title' => 'required|min:5',
            'g-recaptcha-response' => 'required|recaptcha'
        ];
    }

    public function translationRules() {
        return [];
    }

    public function authorize() {
        return true;
    }

    public function messages() {
        return [
            'name.required' => trans('core::core.field_is_required'),
            'email.required' => trans('core::core.field_is_required'),
            'email.email' => 'Błędny format pola',
            'price_brutto.required' => trans('core::core.field_is_required'),
            'title.required' => trans('core::core.field_is_required'),
            'title.min' => 'Pytanie powinno posiadać więcej znaków',
            'g-recaptcha-response.required' => 'Potwierdź że nie jesteś robotem',
            'g-recaptcha-response.recaptcha' => 'Potwierdź że nie jesteś robotem'
        ];
    }

    public function translationMessages() {
        return [];
    }

}