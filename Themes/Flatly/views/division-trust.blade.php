<div class="position-left">
    <h1>
        {{ trans('page::pages.menu.theytrustedus') }}
    </h1>
</div>
<div class="container">
    @if($data['company'])
    @include('sliders.partners',['elements'=>$data['company']])
    @endif

    @if(isset($data['files']))
    @include('sliders.files',['elements'=>$data['files']]) 

    @endif
</div>

<script>

    function makeSwiper(name, par)
    {//Must be in html:
        //Main container class: swiper-container-your-name
        //wrapper class: swiper-wrapper-your-name, swiper-pagination, swiper-button-prev and next,
        //your slideClass defined in par
        var containerClass = '.swiper-container-' + name;
        if ($(containerClass).length) {

            par.wrapperClass = 'swiper-wrapper-' + name;
//            par.prevButton = '.swiper-button-prev-' + name;
//            par.nextButton = '.swiper-button-next-' + name;
            par.pagination = '.swiper-pagination-' + name;
//            par.preventClicks=false; // mouse button up - makes click event
            par.preventClicksPropagation = false;
            return new Swiper(containerClass, par);
        }
        return false;
    }

    $(function () {
        var pp = {};
//        pp.prevButton = null;
//        pp.nextButton = null;
        pp.slideClass = 'swiper-slide';
        pp.pagination = null;
        pp.slidesPerView = 5;
        pp.loop = true;
        pp.spaceBetween = 20;
        pp.autoplay = 4000;
        pp.speed = 4000;
        pp.breakpoints = {
            // when window width is <= 320px
            480: {
                slidesPerView: 3,
                spaceBetween: 0
            },
            // when window width is <= 480px
            768: {
                slidesPerView: 4,
                spaceBetween: 0
            },
            992: {
                slidesPerView: 5,
                spaceBetween: 0
            },
            1200: {
                slidesPerView: 7,
                spaceBetween: 0
            }
        };
        makeSwiper('partners', pp);
        pp = {};
//        pp.prevButton = null;
//        pp.nextButton = null;
        pp.slideClass = 'swiper-slide';
        pp.pagination = null;
        pp.slidesPerView = 7;
        pp.loop = true;
        pp.spaceBetween = 20;
        pp.autoplay = 4000;
        pp.speed = 4000;

        pp.breakpoints = {
            // when window width is <= 320px
            480: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is <= 480px
            768: {
                slidesPerView: 5,
                spaceBetween: 0
            },
            992: {
                slidesPerView: 6,
                spaceBetween: 0
            },
            1200: {
                slidesPerView: 7,
                spaceBetween: 0
            }
        };
        makeSwiper('files', pp);

    });
</script>