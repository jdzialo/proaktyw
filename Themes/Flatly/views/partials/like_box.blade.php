<div class="info-box">
    <a href="https://www.linkedin.com/company/proaktyw?trk=ppro_cprof" target="_blank">
        <div class="button-info in-icon">
            <i class="icon-linkedin"></i>
        </div>
    </a>
    <a href="https://www.instagram.com/proaktyw/" target="_blank">
        <div class="button-info instagram">
            <i class="icon-instagram-1"></i>
        </div>
    </a>
    <a href="https://pl-pl.facebook.com/ProAktyw" target="_blank">
        <div class="button-info facebook">
            <i class="icon-facebook"></i>
        </div>
    </a>
    <!--    <div class="button-info youtube">
            <i class="icon-youtube-play"></i>
        </div>-->
</div>

