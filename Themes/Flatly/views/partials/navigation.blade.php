<nav class="navbar navbar-default navbar-fixed-top">

    <div class="navbar-header  ">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ URL::to('/') }}"> 
            <img class="img-responsive" src="/themes/flatly/img/logo.png">
             <img class='hidden-xs hidden-sm hidden-md img-responsive limits-image '  src="/themes/flatly/img/ograniczenia.png">
        </a>


    </div>
    <div class="navbar-collapse  collapse navbar-responsive-collapse ">

        <div class="pull-right">
            <div class="top-menu myriad">
                {!! Menu::get('Menu') !!}
            </div>
            <div class="languages">
                <span class="slash hidden-sm hidden-xs">&frasl;</span>
                <a href="/pl"><img  src="/themes/flatly/img/pl.jpg"></a>
                <!--<a href="/en"><img  src="/themes/flatly/img/en.jpg"></a>-->
            </div>
        </div>

    </div>
</nav>
<script>
    $(function () {
        console.log($(window).width());
        if ($(window).width() < 768)
        {
            $('.top-menu a').each(function () {
                console.log('ds');
                $(this).click(function () {
                    $('.navbar-fixed-top .navbar-toggle').click();
                });
            });
        }
    });
</script>

