<!DOCTYPE html>
<html>
    <head lang="{{ LaravelLocalization::setLocale() }}">
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="Author" content="Moonbite Agency [MB] - Ivan Dublianski, Kamil Bielak" />

        @if(env('APP_ENV') == 'local')
        <meta name="robots" content="noindex, nofollow" />
        @endif

        @section('meta')
        <meta name="description" content="@setting('core::site-description')" />
        @show

        <title>
            @section('title')@setting('core::site-name')@show
        </title>
        <link rel="shortcut icon" href="{{ Theme::url('favicon.ico') }}">

        {!! Theme::style('css/main.css?v=4') !!}
        <!--<script src="{{ asset('themes/adminlte/vendor/jquery/jquery.min.js') }}"></script>-->
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

        <link rel="stylesheet" href="/themes/flatly/vendor/grid-gallery/css/CSTiles-1.1.0.css">
        <link rel="stylesheet" href="/themes/flatly/vendor/photoswipe/photoswipe.css"> 
        <link rel="stylesheet" href="/themes/flatly/vendor/photoswipe/default-skin/default-skin.css"> 
        <link rel="stylesheet" href="/themes/flatly/vendor/scroll-effects/animate.css"> 
        {!! Theme::style('vendor/swiper/dist/css/swiper.min.css') !!}

    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="50">

        @include('partials.navigation')

        @yield('content')

        @include('partials.footer')



        {!! Theme::script('js/all.js') !!}
        {!! Theme::script('vendor/grid-gallery/js/jquery.CSTiles-1.1.0.js') !!}
        {!! Theme::script('vendor/photoswipe/photoswipe.min.js') !!}
        {!! Theme::script('vendor/photoswipe/photoswipe-ui-default.min.js') !!}
        {!! Theme::script('vendor/swiper/dist/js/swiper.min.js') !!}
        {!! Theme::script('vendor/parallax.js-1.4.2/parallax.min.js') !!}
        {!! Theme::script('vendor/scroll-effects/viewportchecker.js') !!}



        @yield('scripts')
        @stack('scripts')

        <?php if (Setting::has('core::analytics-script')): ?>
            {!! Setting::get('core::analytics-script') !!}
        <?php endif; ?>
<!--               <script src="/themes/flatly/vendor/grid-gallery/js/jquery.CSTiles-1.1.0.min.js"></script>
<script src="/themes/flatly/vendor/grid-gallery/js/jquery.CSShare-1.0.0.min.js"></script>-->
        <!--Start of LeadFinder widget !-->
        <script type="text/javascript">
        var _lfuid = 'e7c70e78d8fcd4d0cbce';
        (function (d) {
            var w = d.createElement('script');
            w.type = 'text/javascript';
            w.asynch = true;
            w.src = '//widget.leadfinder.pl/widget/widget.js';
            var s = d.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(w, s);
        })(document);
        </script>
        <!--End of LeadFinder widget !-->
    </body>
</html>
