
@if(!$elements->isEmpty())

<div class="section files">
    <div class="container">
        <!--        <div class="section-heade                    r">
                <h2 class="title">{{-- trans('messages.files.out_files'                ) --}}</h2>
    </div>-->
        <div class="section-content swiper-container-files">
            <div class="row swiper-wrapper-files">
                @foreach($elements as $partner)
                <div class="swiper-slide">
                    <div class="partner" title="{{ $partner->title }}">
                        <a href="{{ $partner->url }}" target="_blank">
                            <div class="logo-box">
                                <img src="{{ $partner->path }}" alt="{{ $partner->filename }}" title="{{ $partner->filename }}" class="img-responsive" /> 
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>    
        </div> 
    </div> 
</div>
@endif



