
@if(!$elements->isEmpty())

<div class="section partners">
    <div class="container">
        <!--        <div class="section-heade                    r">
                <h2 class="title">{{-- trans('messages.partners.out_partners'                ) --}}</h2>
    </div>-->
        <div class="section-content swiper-container-partners">
            <div class="row swiper-wrapper-partners">
                @foreach($elements as $partner)
                <div class="swiper-slide partners-slide">
                    <div class="partner" title="{{ $partner->title }}">
                        <!--<a href="{{-- $partner->url --}}" target="_blank">-->
                            <div class="logo-box logo-box-company">
                                <img src="{{ $partner->path }}" alt="{{ $partner->filename }}" title="{{ $partner->filename }}" class="img-responsive" /> 
                            </div>
                        <!--</a>-->
                    </div>
                </div>
                @endforeach
            </div>    
        </div> 
    </div> 
</div>
@endif



