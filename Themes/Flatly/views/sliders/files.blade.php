
@if(!$elements->isEmpty())

<div class="section files">
    <div class="container">
        <div class="section-content swiper-container-files">
            <div class="row swiper-wrapper-files">
                @foreach($elements as $partner)
                <div class="swiper-slide">

                    <div class="zoom-gallery">
                        <a href="{{ $partner->path }}" data-source="{{ $partner->path }}">
                            <div class="logo-box">
    <!--<img src="/assets/media/template-pro.png" alt="{{ $partner->path }}" title="{{ $partner->filename }}" class="img-responsive" />--> 
                                <img src="{{ $partner->path }}" alt="{{ $partner->filename }}" title="{{ $partner->filename }}" class="img-responsive" /> 
                            </div>
                        </a>
                    </div>

                </div>
                @endforeach
            </div>    
        </div> 
    </div> 
</div>
@endif

@push('scripts')
{!! Theme::script('vendor/magnific-popup/dist/jquery.magnific-popup.min.js') !!}
<script>
    $(document).ready(function () {
        $('.zoom-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
                verticalFit: true,
                titleSrc: function (item) {
                    return item.el.attr('title') + ' &middot;';
                }
            },
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function (element) {
                    return element.find('img');
                }
            }

        });
    });
</script>
@endpush



