@extends('layouts.master')

@section('title')
    {{ trans('messages.newsletter.subscribe_our_newsletter') }} | @parent
@endsection
@section('meta')
    <meta name="robots" content="noindex, nofollow" />
@endsection

@section('content')
<div class="subpage newsletter-subscribe">
    <div class="breadcrumb-container breadcrumbs">
        <div class="container">
            <div class="shape gray-shape"></div>
        </div>
    </div>
    <div class="section add-to-subscribe">
        <div class="container">
            <form method="POST" action="{{ URL::route('newsletter.subscribe.store') }}" class="subscribe-form">
                {{ csrf_field() }}
                <div class="form-group">
                    <input name="email" type="email" class="form-control" placeholder="E-mail" value="{{ $email }}" required />
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input name="first_name" type="text" class="form-control" placeholder="{{ trans('messages.newsletter.first_name') }}" value="" required />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input name="last_name" type="text" class="form-control" placeholder="{{ trans('messages.newsletter.last_name') }}" value="" required />
                        </div>
                    </div>
                </div>
                <div class="btn-container">
                    <button type="submit" class="black-btn">{{ trans('messages.newsletter.subscribe') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection