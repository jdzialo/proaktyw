
@include('dialogs.get-offer')

<div class="swiper-container-offer">
    <div class="swiper-wrapper-offer">
        <?php
        ?>
        @foreach($offersSorted as $offer)
        <?php
        $background = null;
        $pdf = null;
        foreach ($offer->files()->get() as $f) {

            if ($f->extension != 'pdf') {
                $background = $f;
            } else
                $pdf = $f;
        }
        if (!isset($background))
            $background = (object) array('path' => '/themes/flatly/img/bg-2.jpg');
        ?>


        @if($background)
        <div class="swiper-slide content-left" style="background: url('{{ $background->path }}');" >

            <div class="slider-block container">
                <div>

                    <div class="slider-content">


                        @if($offer->content)
                        <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 half padd'>{!! $offer->body or '' !!}</div>

                        <div class='col-lg-6 col-md-6 col-sm-12 col-xs-12 half'>
                            {!! $offer->content or '' !!}

                        </div>
                        @else
                        <div class='col-lg-12 full'>
                            {!! $offer->body or '' !!} 

                        </div>
                        @endif

                    </div>
                    @if($pdf)
                    <div class="pull-right  col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <a target="_blank" class='btn btn-get' href='{{$pdf->path}}'>
                            {{ trans('page::pages.download.button') }}
                        </a>
                        <!--<button id="get-offer" data-toggle="modal" data-target="#myModal"></button>-->
                    </div>
                    @endif
                </div>
            </div>

        </div>
        @endif
        @endforeach

    </div>

    <div class="swiper-pagination-header"></div>

    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>

<!--            <div class="swiper-button-prev trans"><span class="glyphicon glyphicon-triangle-left"></span></div>
            <div class="swiper-button-next trans"><span class="glyphicon glyphicon-triangle-right"></span></div>-->
    <!--<div class="shape gray-shape"></div>-->

</div>
<!--<div class='contact-with-us pull-right btn-contact hidden-xs'>
    <div>{{-- trans('page::pages.contact.button') --}}! 
        <div class="optional">Opcjonalny opis kontaktu <br> nr telefonu <br> komunikatora 
        </div>
    </div><i class='icon-phone-2'></i>
</div>-->
@include('partials.bottom-menu')








@push('scripts')
<script>
    $(function () {

        var slider = makeSwiper('offer', {
            paginationClickable: true,
            spaceBetween: 0,
            effect: 'slide', //fit
            speed: 500,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
//            loop: true,
            initialSlide: 0
        });
        slider.on('onSlideChangeStart', function (par) {
            var index = slider.activeIndex;
            console.log(index);
            $('.bottom-menu li.active').removeClass('active');
            $('.bottom-menu [slider-control=' + index + ']').parent().addClass('active');
        });


        $('.bottom-menu a').each(function () {
            $(this).on('click', function () {
                slider.slideTo($(this).attr('slider-control'), 400);
            });
        });

//         $('.swiper-wrapper-offer .swiper-slide').parallax();
//        var obj=$('.swiper-wrapper-offer .swiper-slide').parallax();
//        console.log(obj);
//        $("#get-offer").click(function () {
//            BootstrapDialog.alert('I want banana!');
//        })


    })
            ;


</script>
@endpush