
<div class='descriptions'>
    <div class="position-left too">
        <h1>{{ trans('page::pages.menu.aboutus') }}</h1>
    </div>
    <div class="container">
        <div class='col-lg-4'>
            <h2>
                @setting('media::title1')
            </h2>
            <div class='red-rect'></div>
            <div class='content'>
                @setting('media::field1')
            </div>
        </div>
        <div class='col-lg-4'>
            <h2>
                @setting('media::title2')
            </h2>
            <div class='red-rect'></div>
            <div class='content'>
                @setting('media::field2')
            </div>
        </div>
        <div class='col-lg-4'>
            <h2>
                @setting('media::title3')
            </h2>
            <div class='red-rect'></div>
            <div class='content'>
                <div class='li' style="margin-top: 0px;">
                    <span class='bull'></span> @setting('media::field31')
                </div>
                <div class='li'>
                    <span class='bull'></span> @setting('media::field32')
                </div>
                <div class='li'>
                    <span class='bull'></span> @setting('media::field33')
                </div>
                <div class='li'>
                    <span class='bull'></span> @setting('media::field34')
                </div>
            </div>
        </div>
    </div>
</div>

<div class='team-block'>
            @include('team.team')
</div>

<!--array:8 [▼
  "page" => Page {#870 ▶}
  "defaultslider" => Collection {#890 ▶}
  "company" => Collection {#908 ▶}
  "files" => Collection {#928 ▶}
  "images" => Collection {#874 ▶}
  "slider" => Collection {#945 ▶}
  "imagy" => Imagy {#832 ▶}
  "workers" => LengthAwarePaginator {#968 ▶}
]-->
