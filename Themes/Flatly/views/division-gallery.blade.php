
<?php
$xTiles = 5;
$maxRows = 3;
$maxRows--;
$sizeLimit = 3;
$limitElements = 16;
$imagesAmount = count($gallery);
$galPages = intval($imagesAmount / $limitElements);
$lastImages = $limitElements - $imagesAmount % $limitElements;
;
//if ($lastImages > 0) {
//    for ($n = 0; $n < $lastImages; $n++) {
//        $gallery[] = $gallery[$n];
//    }
//}
$imagesAmount = $galPages * $limitElements;
?>
<script>
    var galleryParts = [];
<?php
for ($d = 0; $d <= $imagesAmount; $d++) {
    ?>
        galleryParts[{{$d}}] = [];
    <?php
}
?>
<?php $i = -1; $portion=-1; ?>
    @foreach ($gallery as $img)
    @if($img->path != "")
<?php
$oryginalSize = getimagesize($img->path);
$i++;$portion++;
$currentPart = intval($i / $limitElements);
if($portion+5>$limitElements)
{
    $currentX=$currentY=1;
}
else
{
$currentX = ($i + 1) % $sizeLimit;
$currentY = ($i) % $sizeLimit;
}
if(!$currentX)$currentX=1;
if(!$currentY)$currentY=1;

if($portion+2>$limitElements)$portion=-1;
?>

    galleryParts[{{$currentPart}}].push({
    src: '{{$img->path}}',
            w: '{{$oryginalSize[0]}}',
            h: '{{$oryginalSize[1]}}',
            thumbURL:"{{Imagy::getThumbnail($img->path, 'galleryThumb')}}",
            largeURL:"{{$img->path}}",
            WxH:"{{$oryginalSize[0]}}x{{$oryginalSize[1]}}",
            tileS:'{{$currentX}},{{$currentY}}'
    });
    @endif
    @endforeach

</script>


<div class="position-left">
    <h1>{{ trans('page::pages.menu.gallery') }}</h1>
</div>


<div class='galleries'>

</div>
<div class='show-more-photos myriad-medium'>{{ trans('page::pages.morepicture.one') }}<div>- - - - - - - - - - - - - - - -</div></div>


<script>
            var currentGallery = 0;
    $(function () {
    //Tworzy uklad kafelek, nowe divy

    displayTilesGallery(0, '#GALERIA .galleries');
    });
    $(document).on('click', '.close-gallery', function(){
        $('.galleries div:first-child').siblings().remove();
        $('.show-more-photos').show();
        $('.close-gallery').remove();
        currentGallery = 0;
    })
            $('.show-more-photos').click(function () {
    currentGallery++;
    displayTilesGallery(currentGallery, '#GALERIA .galleries');
    });
    function displayTilesGallery(number, locator)
    {

//    console.log(galleryParts);
    var galleryContent = '<div class="gallery-' + number + '"><div data-cstiles-size="{{$xTiles}},auto" data-cstiles-margin="5" class="cstiles pswp__container">';
    for (var key in galleryParts[number])
    {

    var el = galleryParts[number][key];
    galleryContent += '<figure temprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject"\n\
        class="cstiles__item pswp__item"\n\
        thumb-url="' + el.thumbURL + '" large-image="' + el.largeURL + '" data-size="' + el.WxH + '" data-cstiles-size="' + el.tileS + '">\n\
    <img  class="cstiles__item-image" src="' + el.thumbURL + '" >\n\
    </figure>';
    }

    galleryContent += "</div></div>";
    $(locator).append(galleryContent);
    var gallery = $(".gallery-" + number + ">.cstiles");
    gallery.CSTiles({shareTile: false, size:[8, 2], adaptivSize :{
    "imac": 5,
            "desktops-huge": 10,
            "desktops-big": 7,
            "desktops": 5,
            "base": 6,
            "tablet-landscape": 6,
            "tablet": 4,
            "phone-landscape": 4,
            "phone": 3,
            "small": 3

    }});
    initPhotoSwipeFromDOM(".gallery-" + number + ">.cstiles", number);
    gallery.find('.cstiles__item-content').append('<div class="dark"><div class="text"><i class="icon-magnifying-glass-2"></i>POWIĘKSZ</div></div>');
    number++;
    if (number > {{$galPages}}){
        $('.show-more-photos').hide();
        $('#GALERIA').append('<div class="close-gallery">{{ trans('page::pages.morepicture.two') }}<div>- - - - - - - - - - - - - - - -</div></div>');
    };
    
    }
</script>