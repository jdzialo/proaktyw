                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 @extends('layouts.master')

@section('title')
{{ $page->title }} | @parent
@stop
@section('meta')
<meta name="title" content="{{ $page->meta_title}}" />
<meta name="description" content="{{ $page->meta_description }}" />
@stop

@section('content')

<div class="row">
    @if(Session::has('message'))
    <div class="alert alert-success" style="text-align: center">
        {{Session::get('message')}}
    </div>
    @endif       
</div>
@include('partials.photoswipe-image-preview')
<?php $offersSorted = $offers->sortBy('id'); ?>
<div class="divisions">
    <div class="division post" id="HOME">
        <div class="dark-transparent">
            @include('division-head')
            @include('partials.like_box')
        </div>
    </div>
    <div class="division post" id="OFERTA">
        <div class="dark-transparent">
            @include('division-offer')
        </div>
    </div>
    <div class="division post" id="GALERIA">
        <div class="white">
            @include('division-gallery')
        </div>
    </div>
    <div class="division post" id="ONAS">
        <div class="white">
            @include('division-about-us')
        </div>
    </div>
    <div class="division post" id="ZAUFALI">
        <div class="white">
            @include('division-trust')
        </div>
    </div>
    <div class="division post" id="KONTAKT">
        <div class="position-left-white weight20" style="">
            <h1>{{ trans('page::pages.menu.contact') }}</h1>
        </div>
        <div class="dark-transparent mod">
            @include('division-contact')
            @include('partials.like_box')
            <div class="newsletter ">
                <div class='container'>
                    <div class='newsletter-discount hidden-xs'>@setting('page::field7')</div>
                    <form class="form-horizontal frm_newsletter" method="POST" action="{{ URL::route('newsletter.subscribe.store') }}">
                        <div class='col header'>
                            <h3>Newsletter</h3>
                        </div>
                        <div class='col input'>
                            <input type="email" name="email" class="" placeholder="{{ trans('newsletter::emails.write_your_email') }}" required />
                        </div>
                        <div class='col button'>
                            <button type="submit" class="btn ">
                                {{ trans('page::pages.newsletter.button') }}
                            </button>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>



</div>
<script>
    $(function () {


        var lastScrollId = 1;

        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    isScrolling = true;
                    lastScrollId++;
                    var currScroll = lastScrollId;
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, null, function () {
                        if (currScroll == lastScrollId)
                            isScrolling = false;
                    });

                    return false;
                }
            }
        });
        $('#HOME').parallax({imageSrc: '/themes/flatly/img/1GLOWNE.jpg'});
//        $('#OFERTA').parallax({imageSrc: '/themes/flatly/img/bg-2.jpg'});
        $('#KONTAKT').parallax({imageSrc: '/themes/flatly/img/6KONTAKT.jpg'});

//        Przewijanie
        jQuery('.post').addClass("hidden").viewportChecker({
            classToAdd: 'visible animated fadeIn ',
            offset: 200
        });

        $(window).scroll(function () {
            var scrolled = $(document).scrollTop().valueOf();
            var container = $('.limits-image');
            var height = $('#HOME').height();
            if (scrolled >= parseInt(height)) {
                console.log('kkj');

                container.css('display', 'block');
            } else {
                container.css('display', 'none');
            }
        });

    });

</script>
@stop




