<?php

return [
'title1' => [
'description' => 'page::settings.title1',
'view' => 'text',
'translatable' => true,
],
'field1' => [
'description' => 'page::settings.field1',
'view' => 'textarea',
'translatable' => true,
],
'title2' => [
'description' => 'page::settings.title2',
'view' => 'text',
'translatable' => true,
],

'field2' => [
'description' => 'page::settings.field2',
'view' => 'textarea',
'translatable' => true,
],
'title3' => [
'description' => 'page::settings.title3',
'view' => 'text',
'translatable' => true,
],
'field3' => [
'description' => 'page::settings.field3',
'view' => 'textarea',
'translatable' => true,
],
'title4' => [
'description' => 'page::settings.title4',
'view' => 'text',
'translatable' => true,
],
'field4' => [
'description' => 'page::settings.field4',
'view' => 'text',
'translatable' => true,
],

'title5' => [
'description' => 'page::settings.title5',
'view' => 'text',
'translatable' => true,
],
'field5' => [
'description' => 'page::settings.field5',
'view' => 'text',
'translatable' => true,
],

'title6' => [
'description' => 'page::settings.title6',
'view' => 'text',
'translatable' => true,
],
'field6' => [
'description' => 'page::settings.field6',
'view' => 'textarea',
'translatable' => true,
],
'title7' => [
'description' => 'page::settings.title7',
'view' => 'text',
'translatable' => true,
],
'field7' => [
'description' => 'page::settings.field7',
'view' => 'textarea',
'translatable' => true,
] ,
'title8' => [
'description' => 'page::settings.title8',
'view' => 'text',
'translatable' => true,
],
'field8' => [
'description' => 'page::settings.field8',
'view' => 'textarea',
'translatable' => true,
]    

];
