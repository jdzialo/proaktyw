<?php

return [
'title1' => [
'description' => 'media::settings.title1',
'view' => 'text',
'translatable' => true,
],

'field1' => [
'description' => 'media::settings.field1',
'view' => 'wysiwyg',
'translatable' => true,
],
'title2' => [
'description' => 'media::settings.title2',
'view' => 'text',
'translatable' => true,
],

'field2' => [
'description' => 'media::settings.field2',
'view' => 'wysiwyg',
'translatable' => true,
],

'title3' => [
'description' => 'media::settings.title3',
'view' => 'text',
'translatable' => true,
],
'field31' => [
'description' => 'media::settings.field31',
'view' => 'wysiwyg',
'translatable' => true,
],
'field32' => [
'description' => 'media::settings.field32',
'view' => 'wysiwyg',
'translatable' => true,
],
'field33' => [
'description' => 'media::settings.field33',
'view' => 'wysiwyg',
'translatable' => true,
],
'field34' => [
'description' => 'media::settings.field34',
'view' => 'wysiwyg',
'translatable' => true,
],
];
